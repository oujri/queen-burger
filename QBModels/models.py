from django.db import models


class Banner(models.Model):
    image_landscape = models.ImageField(null=True, verbose_name="Image Bureau")
    image_portrait = models.ImageField(null=True, verbose_name="Image Téléphone")
    link_url = models.URLField(max_length=50, default='#', blank=True, null=True, verbose_name="Lien")
    link_text = models.CharField(max_length=50, blank=True, null=True, verbose_name="Texte")
    creation_date = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Bannière"
        verbose_name_plural = "Bannières"


class Category(models.Model):
    name = models.CharField(max_length=100)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Allergen(models.Model):
    title = models.CharField(max_length=255, verbose_name="Titre")
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Allergie"
        verbose_name_plural = "Allergies"


class Addition(models.Model):
    title = models.CharField(max_length=255, verbose_name="Titre")
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Ajout sur boissons"
        verbose_name_plural = "Ajouts sur boissons"


class Product(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom")
    description = models.TextField(max_length=1000, null=True, blank=True)
    image = models.ImageField(null=True, verbose_name="Image principale")
    image_details = models.ImageField(null=True, verbose_name="Image de détails")
    price = models.FloatField(null=True, blank=True, verbose_name="Prix")
    creation_date = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Category, null=True, on_delete=models.SET_NULL, verbose_name="Catégorie")
    allergens = models.ManyToManyField(Allergen, blank=True, verbose_name="Allergies")
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Produit"
        verbose_name_plural = "Produits"


class Drink(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom")
    description = models.TextField(max_length=1000, null=True, blank=True)
    nutritional_info = models.TextField(max_length=1000, null=True, blank=True, verbose_name="Infos nutritionnelles")
    image = models.ImageField(null=True, verbose_name="Image principale")
    image_details = models.ImageField(null=True, verbose_name="Image de détails")
    price = models.FloatField(null=True, blank=True, verbose_name="Prix")
    creation_date = models.DateTimeField(auto_now_add=True)
    allergens = models.ManyToManyField(Allergen, blank=True, verbose_name="Allergies")
    additions = models.ManyToManyField(Addition, blank=True, verbose_name="Ajouts")
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Boisson"
        verbose_name_plural = "Boissons"


class MenuChoice(models.Model):
    title = models.CharField(max_length=255, verbose_name="Titre")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Choix des menus"
        verbose_name_plural = "Choix des menu"


class Menu(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nom")
    description = models.TextField(max_length=1000)
    image = models.ImageField(verbose_name="Image principale")
    image_details = models.ImageField(verbose_name="Image de détails")
    price = models.FloatField(null=True, verbose_name="Prix")
    creation_date = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)
    allergens = models.ManyToManyField(Allergen, blank=True, verbose_name="Allergies")
    choices = models.ManyToManyField(MenuChoice, blank=True, verbose_name="Choix")
    double_size = models.BooleanField(default=True, verbose_name="Contient double size")
    child_menu = models.BooleanField(default=False, verbose_name="Contient menu enfants")

    def __str__(self):
        return self.name

    def maxi_price(self):
        return self.price + 0.60

    class Meta:
        verbose_name = "Menu"
        verbose_name_plural = "Menus"


class Promos(models.Model):
    image = models.ImageField()
    active = models.BooleanField(default=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    start_date = models.DateField(verbose_name="Date de début")
    end_date = models.DateField(verbose_name="Date de fin")

    def __str__(self):
        return self.image.name

    class Meta:
        verbose_name = "Promotion"
        verbose_name_plural = "Promotions"


class Video(models.Model):
    link = models.URLField(verbose_name="Lien de la vidéo")
    creation_date = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Vidéo"
        verbose_name_plural = "Vidéos"
