from django.contrib import admin
from QBModels.models import *
from django.utils.safestring import mark_safe
from django.contrib.auth.models import User, Group


admin.site.site_header = "Queen Burger administration"
admin.site.site_title = "Queen Burger administration"
admin.site.index_title = "Bienvenue au Queen Burger administration"


class BannerAdmin(admin.ModelAdmin):
    readonly_fields = ["image_bureau_", "image_telephone_", "creation_date"]
    list_display = ('image_bureau', 'link_url', 'active')
    list_display_links = ('image_bureau', )
    actions = ["activer", "desactiver"]

    def activer(self, request, queryset):
        queryset.update(active=True)
    activer.short_description = "Activer la sélection"

    def desactiver(self, request, queryset):
        queryset.update(active=False)
    desactiver.short_description = "Désactiver la sélection"

    # method to remove delete action
    '''def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions'''

    @staticmethod
    def image_bureau_(obj):
        if obj.image_landscape:
            return mark_safe(
                '<img src="{url}" width="{width}" height={height} style="max-width: 600px; max-height: 300px" />'.format(
                    url=obj.image_landscape.url,
                    width=obj.image_landscape.width,
                    height=obj.image_landscape.height,
                )
            )

    @staticmethod
    def image_bureau(obj):
        if obj.image_landscape:
            return mark_safe('<img src="{url}" width="80px" height="40px" />'.format(
                url=obj.image_landscape.url
                )
            )

    @staticmethod
    def image_telephone_(obj):
        if obj.image_portrait:
            return mark_safe('<img src="{url}" width="{width}" height={height} style="max-width: 200px; max-height: 300px" />'.format(
                url=obj.image_portrait.url,
                width=obj.image_portrait.width,
                height=obj.image_portrait.height,
                )
            )


admin.site.register(Banner, BannerAdmin)


class AllergenAdmin(admin.ModelAdmin):
    readonly_fields = ["creation_date"]
    list_display = ('title', )
    list_display_links = ('title', )
    search_fields = ['title']


admin.site.register(Allergen, AllergenAdmin)


class AdditionAdmin(admin.ModelAdmin):
    readonly_fields = ["creation_date"]
    list_display = ('title', )
    list_display_links = ('title', )
    search_fields = ['title']


admin.site.register(Addition, AdditionAdmin)


class ProductAdmin(admin.ModelAdmin):
    readonly_fields = ["image_", "image_de_details_", "creation_date"]
    search_fields = ['name', 'price']
    list_display = ('name','image__',  'price', 'category', 'active')
    list_display_links = ("name", "image__")
    list_filter = ('category', 'active', 'allergens')
    actions = ["activer", "desactiver"]

    def activer(self, request, queryset):
        queryset.update(active=True)
    activer.short_description = "Activer la sélection"

    def desactiver(self, request, queryset):
        queryset.update(active=False)
    desactiver.short_description = "Désactiver la sélection"

    @staticmethod
    def image_(obj):
        if obj.image:
            return mark_safe('<img src="{url}" width="{width}" height={height} style="max-width: 300px; max-height: 300px" />'.format(
                url=obj.image.url,
                width=obj.image.width,
                height=obj.image.height,
                )
            )

    @staticmethod
    def image__(obj):
        if obj.image:
            return mark_safe('<img src="{url}" width="60px" height="60px" />'.format(
                url=obj.image.url
                )
            )

    @staticmethod
    def image_de_details_(obj):
        if obj.image_details:
            return mark_safe('<img src="{url}" width="{width}" height={height} style="max-width: 300px; max-height: 300px" />'.format(
                url=obj.image_details.url,
                width=obj.image_details.width,
                height=obj.image_details.height,
                )
            )


admin.site.register(Product, ProductAdmin)


class MenuChoiceAdmin(admin.ModelAdmin):
    list_display = ('title', )
    list_display_links = ('title', )
    search_fields = ['title']


admin.site.register(MenuChoice, MenuChoiceAdmin)


class MenuAdmin(admin.ModelAdmin):
    readonly_fields = ["image_", "image_de_details_", "creation_date"]
    search_fields = ['name', 'price']
    list_display = ('name','image__',  'price', 'double_size', 'child_menu', 'active')
    list_display_links = ("name", "image__")
    list_filter = ('active', 'allergens', 'double_size', 'child_menu')
    actions = ["activer", "desactiver"]

    def activer(self, request, queryset):
        queryset.update(active=True)
    activer.short_description = "Activer la sélection"

    def desactiver(self, request, queryset):
        queryset.update(active=False)
    desactiver.short_description = "Désactiver la sélection"

    @staticmethod
    def image_(obj):
        if obj.image:
            return mark_safe('<img src="{url}" width="{width}" height={height} style="max-width: 300px; max-height: 300px" />'.format(
                url=obj.image.url,
                width=obj.image.width,
                height=obj.image.height,
                )
            )

    @staticmethod
    def image__(obj):
        if obj.image:
            return mark_safe('<img src="{url}" width="60px" height="60px" />'.format(
                url=obj.image.url
                )
            )

    @staticmethod
    def image_de_details_(obj):
        if obj.image_details:
            return mark_safe('<img src="{url}" width="{width}" height={height} style="max-width: 300px; max-height: 300px" />'.format(
                url=obj.image_details.url,
                width=obj.image_details.width,
                height=obj.image_details.height,
                )
            )


admin.site.register(Menu, MenuAdmin)


class DrinkAdmin(admin.ModelAdmin):
    readonly_fields = ["image_", "image_de_details_", "creation_date"]
    search_fields = ['name', 'price']
    list_display = ('name','image__',  'price', 'active')
    list_display_links = ("name", "image__")
    list_filter = ('active', 'allergens')
    actions = ["activer", "desactiver"]

    def activer(self, request, queryset):
        queryset.update(active=True)
    activer.short_description = "Activer la sélection"

    def desactiver(self, request, queryset):
        queryset.update(active=False)
    desactiver.short_description = "Désactiver la sélection"

    @staticmethod
    def image_(obj):
        if obj.image:
            return mark_safe('<img src="{url}" width="{width}" height={height} style="max-width: 300px; max-height: 300px" />'.format(
                url=obj.image.url,
                width=obj.image.width,
                height=obj.image.height,
                )
            )

    @staticmethod
    def image__(obj):
        if obj.image:
            return mark_safe('<img src="{url}" width="60px" height="60px" />'.format(
                url=obj.image.url
                )
            )

    @staticmethod
    def image_de_details_(obj):
        if obj.image_details:
            return mark_safe('<img src="{url}" width="{width}" height={height} style="max-width: 300px; max-height: 300px" />'.format(
                url=obj.image_details.url,
                width=obj.image_details.width,
                height=obj.image_details.height,
                )
            )


admin.site.register(Drink, DrinkAdmin)


class PromosAdmin(admin.ModelAdmin):
    readonly_fields = ["image_", "creation_date"]
    list_display = ('image__',  'start_date', 'end_date')
    list_display_links = ("image__", )
    list_filter = ('start_date', 'end_date')
    actions = ["activer", "desactiver"]

    def activer(self, request, queryset):
        queryset.update(active=True)
    activer.short_description = "Activer la sélection"

    def desactiver(self, request, queryset):
        queryset.update(active=False)
    desactiver.short_description = "Désactiver la sélection"

    @staticmethod
    def image_(obj):
        if obj.image:
            return mark_safe('<img src="{url}" width="{width}" height={height} style="max-width: 300px; max-height: 300px" />'.format(
                url=obj.image.url,
                width=obj.image.width,
                height=obj.image.height,
                )
            )

    @staticmethod
    def image__(obj):
        if obj.image:
            return mark_safe('<img src="{url}" width="60px" height="60px" />'.format(
                url=obj.image.url
                )
            )


admin.site.register(Promos, PromosAdmin)


class VideoAdmin(admin.ModelAdmin):
    readonly_fields = ["creation_date", "video_"]
    list_display = ('link', 'active')
    list_display_links = ('link', )
    actions = ["activer", "desactiver"]
    search_fields = ["link"]
    list_filter = ('active', )

    def activer(self, request, queryset):
        queryset.update(active=True)
    activer.short_description = "Activer la sélection"

    def desactiver(self, request, queryset):
        queryset.update(active=False)
    desactiver.short_description = "Désactiver la sélection"

    @staticmethod
    def video_(obj):
        if obj.link:
            link = obj.link
            link = link.split('?v=')[1]
            link = link.split('&')[0]
            return mark_safe('<iframe width="560" height="315" src="https://www.youtube.com/embed/{url}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'.format(
                url=link
                )
            )

admin.site.register(Video, VideoAdmin)
admin.site.unregister(User)
admin.site.unregister(Group)
