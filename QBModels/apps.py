from django.apps import AppConfig


class QbmodelsConfig(AppConfig):
    name = 'QBModels'
    verbose_name = 'Queen Burger'
