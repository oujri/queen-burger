# Generated by Django 2.1.7 on 2019-04-12 16:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('QBModels', '0018_auto_20190412_1617'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menu',
            name='image',
            field=models.ImageField(default='aa', upload_to=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='menu',
            name='image_details',
            field=models.ImageField(default='aa', upload_to=''),
            preserve_default=False,
        ),
    ]
