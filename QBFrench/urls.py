from django.urls import path

from QBFrench import views

app_name = 'qbFrench'

urlpatterns = [
    path('', views.index, name='home'),
]
