

from django.shortcuts import render
from django.utils import timezone

from QBModels.models import *


def index(request):
    banners = Banner.objects.order_by('creation_date').filter(active=True)[:5]
    burgers = Product.objects.filter(category__name='Burgers').filter(active=True)
    fingers = Product.objects.filter(category__name='FingerFood').filter(active=True)
    salades = Product.objects.filter(category__name='Salades').filter(active=True)
    sauces = Product.objects.filter(category__name='Sauces').filter(active=True)
    desserts = Product.objects.filter(category__name='Dessert').filter(active=True)
    drinks = Drink.objects.filter(active=True)
    menus = Menu.objects.filter(active=True)
    videos = Video.objects.filter(active=True)
    promos = Promos.objects.order_by('-creation_date').filter(active=True).filter(end_date__gte=timezone.now()).filter(
        start_date__lte=timezone.now())[:2]

    context = {
        'banners': banners,
        'burgers': burgers,
        'fingers': fingers,
        'salades': salades,
        'sauces': sauces,
        'desserts': desserts,
        'drinks': drinks,
        'menus': menus,
        'promos': promos,
        'videos': videos,
    }

    return render(request, 'french/home.html', context)
